#!/usr/bin/env bash



BASE_DIR="$( cd "$(dirname "$0")" ; pwd -P )"

TARGET_HOST=sb-uwf2.swissbib.unibas.ch
USER=swissbib
TARGET_DIR=/tmp
IMAGETAR=image_archive.tar
IMAGENAME=dummy-metadata-consumer
IMAGETAG=latest

cd $BASE_DIR

echo "build latest image gesamtexport"

docker build -t $IMAGENAME:$IMAGETAG .

echo "save latest image gesamtexport as tar file"
docker save $IMAGENAME:$IMAGETAG --output $IMAGETAR

echo "cp tar file to target host"
scp $IMAGETAR $USER@$TARGET_HOST:$TARGET_DIR

echo "stop container if running on the target host"
ssh $USER@$TARGET_HOST "cd $TARGET_DIR; docker container stop $IMAGENAME:$IMAGETAG"

echo "rm already existing image o target host"
echo "load just created image on target host"
ssh $USER@$TARGET_HOST "cd $TARGET_DIR; docker image rm $IMAGENAME:$IMAGETAG; docker load --input $IMAGETAR"

rm $IMAGETAR

ssh $USER@$TARGET_HOST "rm $TARGET_DIR/$IMAGETAR"
