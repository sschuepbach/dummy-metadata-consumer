FROM openjdk:8
ADD . /
WORKDIR /
RUN ./gradlew -q --no-scan --no-daemon --no-build-cache shadowJar

FROM openjdk:8-jre-alpine
COPY --from=0 /build/libs/app.jar /app/
CMD java -jar /app/app.jar
