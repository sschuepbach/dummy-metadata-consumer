# Dummy Metadata Consumer

A consumer whose only purpose is to output the data it receives.

## Configuration

* `BOOTSTRAP_SERVERS`: Comma-separated list of brokers
* `GROUP_ID`: Name of consumer group
* `TOPICS`: Comma-separated list of input topics